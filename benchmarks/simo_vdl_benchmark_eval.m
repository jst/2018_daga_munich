addpath( 'ini2struct' )
bm_ini = ini2struct( 'ITADSP_BM_SIMO_VDL.ini' );
fs = 44100;


%% Plot
figure
hold on

title( 'SIMO VDL c-spline interpolated read-out performance for 256 paths' )
xlabel( 'Resampling factor' )
ylabel( 'Computation time mean and stddev / ms' )
ylim( [ 0 4 ] )
xlim( [ 0.45 1.55 ] );
grid on

Bs = [ 32 64 128 256 ];
plot_shift = -0.002;

for b = Bs

    % Get data
    bm_ids = fieldnames( bm_ini );
    R = zeros( numel( bm_ids ), 1 );
    M = zeros( numel( bm_ids ), 1 );
    E = zeros( numel( bm_ids ), 1 );
    for n = 1:numel( bm_ids );
        bm_id = bm_ids{ n };
        bm = bm_ini.( bm_id );
        B = str2num( bm.blocklength );
        if B == b && strcmpi( bm.interpolationtype, 'cubicspline' )
            delay_inc = str2num( bm.delayincrementsample );
            R( n ) = 1 + delay_inc / B;
            M( n ) = str2num( bm.computationmean );
            E( n ) = str2num( bm.computationstddev );
        end
    end
    
    P = [ R( R > 0 ), M( R > 0 ), E( R > 0 ) ];
    [ P_sorted, P_sorted_idx ] = sort( P( :, 1 ), 1 );
    errorbar( P_sorted + plot_shift, P( P_sorted_idx, 2 ) * 1e3, P( P_sorted_idx, 3 ) * 1e3, 'O-', 'LineWidth', 1 ) % plot in ms over resampling factor
    
    plot_shift = plot_shift + 0.0025;
end


legend(  'Location', 'northeast', ...
 'Blocklength: 32 samples  (0.7 ms time budget)', ...
 'Blocklength: 64 samples  (1.5 ms time budget)', ...
 'Blocklength: 128 samples (2.9 ms time budget)', ...
 'Blocklength: 256 samples (5.8 ms time budget)' )

set( gcf, 'units', 'points', 'position', [ 1, 1, 800, 400 ] )
print( 'simo_vdl_benchmark','-dpdf', '-bestfit' )


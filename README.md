# Purpose

This repository has been created to provide supporting information for

```
Title: Real-time auralization of propagation paths with reflection, diffraction and Doppler shift

Conference: DAGA 2018, München, Germany

Session: Virtuelle Akustik

Authors: Jonas Stienen and Michael Vorländer

```

Copyright Jonas Stienen, 2018. Media is [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/) and code is [Apache License Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).
See also LICENSE files in the respective directories.


# Code 

## Matlab

Some parts require the [ITA-Toolbox](https://git.rwth-aachen.de/ita/toolbox).

## C++

See [ITACoreLibs](https://git.rwth-aachen.de/ita/ITACoreLibs) and [ITADSP](https://git.rwth-aachen.de/ita/ITADSP), where the SIMO VDL can be found. For running benchmarks, the corresponding flag has to be activated in CMake.

The used versions of the code is linked as submodules in the `code` directory. Use CMake and the CMakeLists.txt in that folder to create a project with all tests and benchmarks.
Clone with `--recursive` flag in order to populate submodules.


# Support

Please understand that no support can be provided for building the code base, if the available guides are not sufficient.
The main purpose why the code is open source is to increase transparency and help other researchers to understand the approach and reproduce results (mostly by inspecting the source code).


# Further information

Find more about me and the Institute of Technical Acoustics (ITA) of the RWTH Aachen University on

[http://www.akustik.rwth-aachen.de](http://www.akustik.rwth-aachen.de) (website)

[http://blog.rwth-aachen.de/akustik](http://blog.rwth-aachen.de/akustik) (Akustik-blog)

%% Doppler-shift limits

f_s = [ 44100 48000 96000 ]; % Hz, Sampling frequencies
f_N = f_s / 2; % Hz,  Nyquist frequencies
f_lA = ita_ANSI_center_frequencies( [ 6000 20000 ], 12 ); % Hz, Aliasing limit
c = 341; % m/s, speed of sound


%% Read-out aliasing

r_lA = repmat( f_N, length( f_lA ), 1 ) ./ repmat( f_lA, length( f_s ), 1 )'; % r < 1, downsampling
v_ss_lA = c * ( 1 - 1 ./ r_lA );

% Plot

figure
semilogx( f_lA / 1e3, v_ss_lA * 3.6, 'Linewidth', 2 )
xlim( [ 6000 20000 ] / 1e3 )

title( 'Aliasing limit for approaching source' )
xlabel( 'Upper frequency limit of emitted source signal / kHz' )
ylabel( 'Maximum allowed sound source velocity / km/h' )
legend( [ 'Sampling frequency ' num2str( f_s( 1 ) ) ' Hz' ], ...
[ 'Sampling frequency ' num2str( f_s( 2 ) ) ' Hz' ], ...
[ 'Sampling frequency ' num2str( f_s( 3 ) ) ' Hz' ], ...
 'Location', 'southwest' )
grid on

set( gcf,'units','points', 'position', [ 1, 1, 800, 400 ] )
print( 'doppler_shift_aliasing','-dpdf', '-bestfit' )


%% Read-out Nyquist audibility


r_lN = repmat( f_lA, length( f_s ), 1 )' ./ repmat( f_N, length( f_lA ), 1 ); % r > 1, upsampling
v_sr_lN = c * ( 1 - r_lN );

% Plot

figure
semilogx( f_lA / 1e3, v_sr_lN * 3.6, 'Linewidth', 2 )
xlim( [ 6000 20000 ] / 1e3 )

title( 'Nyquist frequency shift for departing receiver' )
xlabel( 'Shifted Nyquist frequency / kHz' )
ylabel( 'Sound receiver velocity / km/h' )
legend( [ 'Sampling frequency ' num2str( f_s( 1 ) ) ' Hz' ], ...
[ 'Sampling frequency ' num2str( f_s( 2 ) ) ' Hz' ], ...
[ 'Sampling frequency ' num2str( f_s( 3 ) ) ' Hz' ], ...
 'Location', 'southwest' )
grid on

set( gcf,'units','points', 'position', [ 1, 1, 800, 400 ] )
print( 'doppler_shift_nyquist','-dpdf', '-bestfit' )

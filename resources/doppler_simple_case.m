%% Doppler-shift simple case

c = 341; % m/s, speed of sound
v_ss = linspace( 0, c ); % m/s, sound source velocity
v_sr = linspace( 0, c ); % m/s, sound receiver velocity


%% Approaching

% Sound source to medium ration
r_ss2m_approaching = 1 ./ ( 1 - v_ss ./ c );
r_m2sr_approaching = 1 + v_sr ./ c;


%% Departing

% Sound source to medium ration
r_ss2m_departing = 1 ./ ( 1 + v_ss ./ c );
r_m2sr_departing = 1 - v_sr ./ c;


%% Plot

figure
hold on
plot( v_ss * 3.6, log2( r_ss2m_approaching ), 'Linewidth', 2 )
plot( v_sr * 3.6, log2( r_m2sr_approaching ), 'Linewidth', 2, 'LineStyle', '--' )
plot( v_ss * 3.6, log2( r_ss2m_departing ), 'Linewidth', 2 )
plot( v_sr * 3.6, log2( r_m2sr_departing ), 'Linewidth', 2, 'LineStyle', '--' )
xlim( [ 0 c ] * 3.6 )
ylim( [ -5 5 ] )

title( 'Simple cases of Doppler shifts for dynamic sound source and sound receiver' )
xlabel( 'Velocity / km/h' )
ylabel( 'Doppler shift / Octave' )
legend(  'Location', 'southwest', ...
'Sound source approaching', ...
'Sound receiver approaching', ...
'Sound source departing', ...
'Sound receiver departing' )
grid on

set( gcf,'units','points', 'position', [ 1, 1, 2000, 1000 ] )
print( 'simple_doppler_shifts','-dpdf', '-bestfit' )

